<?php
/**
 * Created by PhpStorm.
 * User: lsmartins
 * Date: 18/10/19
 * Time: 18:01
 */

namespace Awm\BrAddress\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install cities
         */
        $data = [
            [
                'message' => 'Happy New Year'
            ],
            ['message' => 'Merry Christmas']
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('greeting_message'), $bind);


        }
    }
}