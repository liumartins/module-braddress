<?php
/**
 * Created by PhpStorm.
 * User: lsmartins
 * Date: 17/10/19
 * Time: 16:36
 */

namespace Awm\BrAddress\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $table = $installer->getConnection()
            ->newTable($installer->getTable('braddress_cities'))
                ->addColumn(
                    'city_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['indentity' => true, 'nullable' => false, 'primary' => true],
                    'ID'
                )
                ->addColumn(
                    'region_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => false],
                    'Region Id'
                )
                ->addColumn(
                    'city_name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false, 'default' => ''],
                    'City Name'
                )
            ->setComment("Brazilian Cities Table");

        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }
}