<?php
/**
 * Created by PhpStorm.
 * User: lsmartins
 * Date: 01/10/19
 * Time: 22:09
 */

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'Awm_BrAddress',
    __DIR__);
